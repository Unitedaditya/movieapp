//
//  MovieViewModel.swift
//  Movie App
//
//  Created by Aditya Yadav on 21/10/20.
//

import Foundation

class MovieViewModel : ObservableObject {
    
    @Published var movies : MovieModel!
    @Published var data = [Result]()
    @Published var count = 1
    @Published var topRatedData = [Result]()
    @Published var trendingData = [Result]()
    @Published var trendingDataLoaded = false
    private var apiKey = "d2cf994786e92920bf7a4fbe77d2c9e7"
    
    init() {
        fetchMovies(isPopular: false)
    }
    
    func fetchMovies(isPopular : Bool){
        
        var apiUrl = String()

        if isPopular {
            apiUrl = "https://api.themoviedb.org/3/discover/movie?api_key=\(apiKey)&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=\(count)"
        } else {
            
            apiUrl = "https://api.themoviedb.org/3/movie/top_rated?api_key=\(apiKey)&language=en-US&page=\(count)"
        }
         
        guard let url = URL(string: apiUrl) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let data = data {
                DispatchQueue.main.async {
                    do{
                        let json = try JSONDecoder().decode(MovieModel.self, from: data)
                        
                        if isPopular {
                            let oldData = self.data
                            DispatchQueue.main.async {
                                self.data = oldData + json.results
                                self.count += 1
                            }
                        } else {
                            let oldData = self.topRatedData
                            DispatchQueue.main.async {
                                self.topRatedData = oldData + json.results
                                self.count += 1
                              
                            }
                        }
                    } catch {
                        print(error.localizedDescription)
                    }
                }
            }
        }.resume()

    }
    
    
    func fetchTrendingMovies(){
        let Apiurl = "https://api.themoviedb.org/3/trending/movie/day?api_key=\(apiKey)"
        
        guard let url = URL(string: Apiurl) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let data = data {
                
                    do{
                        let json = try JSONDecoder().decode(MovieModel.self, from: data)

                        DispatchQueue.main.async {
                            for index in (0..<5){
                                self.trendingData.append(json.results[index])
                            }
                            self.trendingDataLoaded = true
                        }
                      
                    } catch {
                        print(error.localizedDescription)
                    }
            }
        }.resume()
    }
    
    func search(query: String){
        for dat in topRatedData{
            if dat.originalTitle.contains(query){
                print(dat.originalTitle)
            }
        }
    }

}
