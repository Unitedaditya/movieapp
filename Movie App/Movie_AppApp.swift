//
//  Movie_AppApp.swift
//  Movie App
//
//  Created by Aditya Yadav on 20/10/20.
//

import SwiftUI

@main
struct Movie_AppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
