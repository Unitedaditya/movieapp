//
//  MovieModel.swift
//  Movie App
//
//  Created by Aditya Yadav on 21/10/20.
//

import Foundation

// MARK: - MovieModel
struct MovieModel: Codable {
    let page, totalResults, totalPages: Int
    let results: [Result]

    enum CodingKeys: String, CodingKey {
        case page
        case totalResults = "total_results"
        case totalPages = "total_pages"
        case results
    }
}

// MARK: - Result
struct Result: Codable , Identifiable , Equatable {
    let posterPath: String
    let id: Int
    let originalTitle: String
    let voteAverage: Double
    let overview : String
    let releaseDate: String?

    enum CodingKeys: String, CodingKey {
        case posterPath = "poster_path"
        case id
        case originalTitle = "original_title"
        case voteAverage = "vote_average"
        case overview
        case releaseDate = "release_date"
    }
}

