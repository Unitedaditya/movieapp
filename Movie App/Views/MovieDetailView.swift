//
//  MovieDetailView.swift
//  Movie App
//
//  Created by Aditya Yadav on 21/10/20.
//

import SwiftUI
import SDWebImageSwiftUI

struct MovieDetailView: View {
    var movie : Result
    
    var body: some View {
        ScrollView {
            VStack {
                WebImage(url: URL(string: "https://image.tmdb.org/t/p/w500/\(movie.posterPath)"))
                    .resizable()
                    .frame(height: 500)
                
                VStack(alignment: .leading) {
                    HStack {
                        Text(movie.originalTitle)
                            .font(.title)
                            .fontWeight(.bold)
                        Spacer()
                    }
                    
                    HStack(spacing: 30){
                        Text("\(movie.releaseDate ?? "NA")")
                            .foregroundColor(.secondary)
                            .fontWeight(.medium)
                        
                        HStack(spacing: 2) {
                            Text("\(String(format: "%.2f", (Double(movie.voteAverage))))")
                                .fontWeight(.medium)
                            
                            Image(systemName: "star.fill")
                                .font(.caption)
                        }
                    }
                    
                    Text(movie.overview)
                        .padding(.top)
                        .fixedSize(horizontal: false, vertical: true)
                    
                }.padding(.leading, 10)
            }
        }.edgesIgnoringSafeArea(.all)
    }
}

struct MovieDetailView_Previews: PreviewProvider {
    static var previews: some View {
        Text("")
    }
}
