//
//  ContentView.swift
//  Movie App
//
//  Created by Aditya Yadav on 20/10/20.
//

import SwiftUI
import SDWebImageSwiftUI

struct ContentView: View {
    
    //MARK: States
    
    @State private var search = ""
    @State private var columns = Array(repeating: GridItem(.flexible() , spacing: 10), count: 3)
    @State private var showMovieDetail = false
    @State private var isPopular = true
    @State private var selected: Result? = nil
    @State private var trendingMoviesLoaded = false
    @State private var showTrendingSheet = false
    @State private var selectedTrending: Result? = nil
    

    //MARK: VIEW MODEL
    
    @ObservedObject var movieViewModel = MovieViewModel()
        
    var body: some View {
        ScrollView(.vertical , showsIndicators: false) {
            LazyVStack{
                HStack{
                    Text("Movies")
                        .font(.title)
                        .fontWeight(.bold)
                        .offset(y: 10)
                    
                    Spacer()
                }
                .padding()
                
                //MARK:- Search Bar
            
                TextField("Search", text: $search)
                    .padding(.vertical , 10)
                    .padding(.horizontal)
                    .background(Color.primary.opacity(0.07))
                    .cornerRadius(10)
                    .padding(.horizontal)
                    
                
                if search != "" {
                    if movieViewModel.data.filter({$0.originalTitle.lowercased().contains(self.search.lowercased())}).count == 0 {
                        
                        VStack {
                            Text("No Results Found")
                        }
                    } else {
                        VStack {
                            ForEach(movieViewModel.data.filter({$0.originalTitle.lowercased().contains(self.search.lowercased())}) , id: \.id){ movie in
                                
                                HStack {
                                    WebImage(url: URL(string: "https://image.tmdb.org/t/p/w500/\(movie.posterPath)"))
                                        .resizable()
                                        .frame(width: 50  , height: 50)
                                    Text(movie.originalTitle)
                                        .onTapGesture{
                                            self.selected = movie
                                            self.showMovieDetail = true
                                    }
                                    
                                    Spacer()
                                }
                                
                            }.padding()
                            .frame(maxHeight: 500)
                        }
                    }
                }
      
                HStack {
                    Text("Trending")
                        .font(.title)
                        .fontWeight(.bold)
                    
                    Spacer()
                }.padding([.leading , .top] , 10)
                
                ScrollView(.horizontal , showsIndicators : false){
                    HStack(alignment: .center , spacing: 0){
                        ForEach(0...4, id: \.self){ index in
                            if movieViewModel.trendingDataLoaded {
                                WebImage(url: URL(string: "https://image.tmdb.org/t/p/w500/\(movieViewModel.trendingData[index].posterPath)"))
                                    .resizable()
                                    .aspectRatio(contentMode: .fill)
                                    .transition(.fade(duration: 0.5))
                                    .frame(width: 320 ,height: 200)
                                    .cornerRadius(15)
                                    .padding(.horizontal)
                                    .tag(index)
                                    .onTapGesture {
                                        
                                        self.selected = movieViewModel.trendingData[index]
                                        self.showMovieDetail = true
                                    }
                            }
                        }
                    }
                }
                .padding(.top)
                .onAppear(perform: {
                    movieViewModel.fetchTrendingMovies()
                })
                
                
                HStack{
                    Text(isPopular ? "Popular" : "Top Rated")
                        .font(.title)
                        .fontWeight(.bold)
                    
                    Spacer()
                    
                    Button(action: {}){
                        Image(systemName: isPopular ? "star.circle.fill" : "flame.fill")
                            .font(.system(size: 24))
                            .foregroundColor(.primary)
                            .onTapGesture(perform: {
                                self.isPopular.toggle()
                                self.movieViewModel.fetchMovies(isPopular: isPopular)
                            })
                    }
                }
                .padding()
                
                //MARK:- Grid View
                
                    LazyVGrid(columns: self.columns){
                        ForEach(isPopular ? movieViewModel.data : movieViewModel.topRatedData , id: \.id){ movie in
                            
                            if movie == movieViewModel.data.last {
                             
                                MovieView(movie: movie, isLast: true, showMovieDetail: $showMovieDetail, isPopular: self.$isPopular, movievm: self.movieViewModel)
                                    .onTapGesture {
                                        
                                        self.selected = movie
                                        self.showMovieDetail = true
                                    }

                            } else {

                                MovieView(movie: movie, isLast: false, showMovieDetail: $showMovieDetail, isPopular: self.$isPopular, movievm: self.movieViewModel)
                                    .onTapGesture {
                                        
                                        self.selected = movie
                                        self.showMovieDetail = true
                                }
                            }
                        }
                     
                    }.padding(.horizontal , 5)
                
            }
            .padding(.vertical)
            
        }
        .sheet(item: $selected) { item in
            MovieDetailView(movie: item)
        }
        .onAppear(perform: {
            movieViewModel.fetchMovies(isPopular: isPopular)
        })
   
        .background(Color.black.opacity(0.05)).edgesIgnoringSafeArea(.all)
    }
}

struct MovieView : View {
    var movie : Result
    var isLast : Bool
    @Binding var showMovieDetail : Bool
    @Binding var isPopular : Bool
    @ObservedObject var movievm : MovieViewModel
    
    //MARK: VIEW MODEL
    
    @ObservedObject var movieViewModel = MovieViewModel()

    var body: some View {
        if self.isLast {
            WebImage(url: URL(string: "https://image.tmdb.org/t/p/w500/\(movie.posterPath)"))
                .resizable()
                .transition(.fade(duration: 0.5))
                .frame(height: 180)
                .shadow(radius: 10)
                .cornerRadius(10)
                .padding(.bottom)
                .onAppear(perform: {
                    self.movievm.fetchMovies(isPopular: true)
                })
        } else {
            
            WebImage(url: URL(string: "https://image.tmdb.org/t/p/w500/\(movie.posterPath)"))
                .resizable()
                .transition(.fade(duration: 0.5))
                .frame(height: 180)
                .cornerRadius(10)
                .shadow(radius: 10)
                .padding(.bottom)
                .onAppear(perform: {
                    self.movievm.fetchMovies(isPopular: isPopular)
                })

        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}


